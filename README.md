This is a bag of words classifier written in python. The first program reads in a TSV file, trains a Multinomial naive bayes classifier, then outputs the classifier and vectorizer. The second file reads in the Vectorizer and classifer, then uses these files to classify the test data, outputing a CSV with the results.

This is a command line program.. here's how to call:

To create the classifier, pipeline, vectorizer vocab, and most important features list :

python MultinomialNB_Trainer.py -i "labeledTrainData.tsv" -c "./Classifier/classifier.pkl" -p "./Pipeline/pipeline.pkl" -v "./Vectorizer/vectorizer.pkl" -m "MultinomialNB_Most_Important_Features.txt"

To use the classifier and vocab to classify a file:

python MultinomialNB_Predictor.py -i "testData.tsv" -p "./Pipeline/pipeline.pkl" -c "./Classifier/classifier.pkl" -v "./Vectorizer/vectorizer.pkl" -o "MultinomialNB_Predicted_model.csv"

This reads in 25000 movie reviews and trains a classifier to recognize sentiment. This bag of words classifier normalizes the vocab over length of document by using the Pipeline object. 

