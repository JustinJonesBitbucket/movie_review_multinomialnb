#!/usr/bin/python
import sys, getopt
import pandas as pd #to read in the data
import nltk
import re
from nltk.corpus import stopwords
from bs4 import BeautifulSoup # to beautify the data
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import metrics
from sklearn.grid_search import GridSearchCV
##use python train_test_doc_splitter.py -i "labeledTrainData.tsv" 
                   
def main(argv):
		
	infile = ''
	outfile_test = 'test.tsv'
	outfile_train = 'train.tsv'
	test_percentage=.2
	train_percentage=.8


	try:
	    opts, args = getopt.getopt(argv,"hi:",["ifile="])
	except getopt.GetoptError:
	    print 'train_test_doc_splitter.py -i <infile> '
	    sys.exit(2)
	for opt, arg in opts:
			if opt == '-h':
				 print 'train_test_doc_splitter.py -i <infile>'
				 sys.exit()
			elif opt in ("-i", "--ifile"):
				 infile = arg
		

	print 'Infile is :', infile



	print "Reading in the training file"
	### read in the data		
	train = pd.read_csv(infile, header=0, \
                delimiter="\t", quoting=3)
	print "Train.shape is:", train.shape
	print "Train Column Values: ",train.columns.values

	
	#print out a review to check read in
	#print train["review"][0]
	#clean_review = review_to_words( train["review"][0] )
	#print clean_review

	#Get the number of reviews based on the dataframe column size
	num_reviews = train["review"].size

	num_reviews = 10 ### Debug code

	num_train_index= int(num_reviews*train_percentage)
	
	num_test_index=num_train_index+1

	print "Print Num Train index is:", num_train_index
	print "Print Num Test index is:", num_test_index 

	train_id=[]
	train_sentiment=[]
	train_review=[]

	for i in range(num_train_index):
		train_id.append(train["id"][i])
		train_sentiment.append(train["sentiment"][i])
		train_review.append(train["review"][i])

	###Write the train file
	raw_data= {'id':train_id, 'sentiment':train_sentiment, 'review':train_review        }		

	df = pd.DataFrame(raw_data, columns=['id','sentiment','review'] )

	df.to_csv( outfile_train,sep= '\t', index=False, header=True, mode='a', encoding='utf-8') # "Terror_Doc_Dataset.csv",

	print "Output the file", outfile_train		


## Write the test file
	start_num = num_test_index
	end_num = num_reviews - num_train_index
	 
	for i in range(num_test_index):
		test_id.append(train["id"][i])
		train_sentiment.append(train["sentiment"][i])
		train_review.append(train["review"][i])







if __name__ == "__main__":
   main(sys.argv[1:])
