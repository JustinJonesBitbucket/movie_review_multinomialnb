#!/usr/bin/python

import sys, getopt
import pandas as pd #to read in the data
import nltk
import re
from nltk.corpus import stopwords
from bs4 import BeautifulSoup # to beautify the data
import numpy as np
from sklearn.externals import joblib
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier

#usage python MultinomialNB_Predictor.py -i "testData.tsv" -p "./Pipeline/pipeline.pkl" -c "./Classifier/classifier.pkl" -v "./Vectorizer/vectorizer.pkl" -o "MultinomialNB_Predicted_model.csv"

def main(argv):

	infile = ''

	outfile=''

	classifier_file=''

	vectorizer_file=''

	pipeline_file=''

	try:
		opts, args = getopt.getopt(argv,"hi:p:c:o:v:",["ifile=","pipeline=","classifier=","vfile=","ofile="])
	except getopt.GetoptError:
		print 'MultinomialNB_Predictor.py -i <ifile.tsv> -p <pipeline.pkl> -c <classifier.pkl> -v <vectorizer.pkl> -o <outfile.csv>'
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print 'MultinomialNB_Predictor.py -i <ifile.tsv> -p <pipeline.pkl> -c <classifier.pkl> -v <vectorizer.pkl> -o <outfile.csv>'
			sys.exit()
		elif opt in ("-i", "--ifile"):
			infile = arg
		elif opt in ("-p", "--pipeline"):
			pipeline_file = arg
		elif opt in ("-c", "--classifier"):
			classifier_file = arg
		elif opt in ("-v", "--vfile"):
			vectorizer_file = arg
		elif opt in ("-o", "--ofile"):
			outfile = arg
	print 'Infile is :', infile
	print 'Outfile is:', outfile
	print "Classifier is:", classifier_file
	print "Vectorizer is:", vectorizer_file
	print "Pipeline is:", pipeline_file

	###load the Pipeline
	pipeline = joblib.load(pipeline_file)
	print "Loaded the Pipeline"
	##load the classifier

	classifier = joblib.load(classifier_file)### put in the name of the classifer, 'filename.pkl'
	print "Loaded the Classifier"
	#load the vectorizer

	vectorizer = joblib.load(vectorizer_file)### put in the name of the classifer, 'filename.pkl'
	print "Loaded the Vectorizer"

	# Read the test data
	test = pd.read_csv(infile, header=0, delimiter="\t", \
	quoting=3 ) #infile is testData.tsv

	# Verify that there are 25,000 rows and 2 columns
	print "Test shape(Rows, Columns of Data):", test.shape

	# Create an empty list and append the clean reviews one by one
	num_reviews = len(test["review"])

	print "Num_reviews is :", num_reviews

	clean_test_reviews = []

	print "Cleaning and parsing the test set...\n"
	for i in xrange(0,num_reviews):
		if( (i+1) % 1000 == 0 ):
			print "Review %d of %d\n" % (i+1, num_reviews)
		clean_review = review_to_words( test["review"][i] )
		clean_test_reviews.append( clean_review )

	print "Length of Clean_Test_Reviews:", len(clean_test_reviews)

	# Initialize the "CountVectorizer" object, which is scikit-learn's
	# bag of words tool.
#	vocabulary_to_load = joblib.load(vectorizer_file)### put in the name of the classifer, 'filename.pkl'
#	print "Loaded Vectorizer:", vectorizer_file
#	vectorizer = CountVectorizer(vocabulary=vocabulary_to_load)

	#### create the data array and the target array
	print "Creating the test data array..."
	data_array=[]

	for i in range(0, num_reviews):
		data_array.append(clean_test_reviews[i])

	test_array = np.asarray(data_array)
	print "Test Array Shape", test_array.shape

	print "Doing the predictions..."
	result = pipeline.predict(test_array)
#	# Get a bag of words for the test set, and convert to a numpy array
#	test_data_features = vectorizer.transform(clean_test_reviews)

#	test_data_features = test_data_features.toarray()
# 	print "Test data feature shape:", test_data_features.shape

#	# Take a look at the words in the vocabulary
#	#vocab = vectorizer.get_feature_names()
#	#print vocab
#	#print "Printed Vocab"

#	# Use the random forest to make sentiment label predictions
#	result = forest.predict(test_data_features)

	# Copy the results to a pandas dataframe with an "id" column and
	# a "sentiment" column
	output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )

	# Use pandas to write the comma-separated output file
	output.to_csv( outfile, index=False, quoting=3 ) # "Bag_of_Words_model.csv",
	print "CSV File output to :", outfile
	print "Job Complete"

def review_to_words( raw_review ):
	# Function to convert a raw review to a string of words
	# The input is a single string (a raw movie review), and
	# the output is a single string (a preprocessed movie review)
	#
	# 1. Remove HTML
	review_text = BeautifulSoup(raw_review).get_text()
	#
	# 2. Remove non-letters
	letters_only = re.sub("[^a-zA-Z]", " ", review_text)
	#
	# 3. Convert to lower case, split into individual words
	words = letters_only.lower().split()
	#
	# 4. In Python, searching a set is much faster than searching
	# a list, so convert the stop words to a set
	stops = set(stopwords.words("english"))
	#
	# 5. Remove stop words
	meaningful_words = [w for w in words if not w in stops]
	#
	# 6. Join the words back into one string separated by space,
	# and return the result.
	return( " ".join( meaningful_words ))

if __name__ == "__main__":
	main(sys.argv[1:])
