#!/usr/bin/python
import sys, getopt
import pandas as pd #to read in the data
import nltk
import re
from nltk.corpus import stopwords
from bs4 import BeautifulSoup # to beautify the data
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import metrics
from sklearn.grid_search import GridSearchCV
from sklearn import cross_validation
from sklearn import datasets
from sklearn import svm
from sklearn.naive_bayes import MultinomialNB


##use python MultinomialNB_Trainer.py -i "labeledTrainData.tsv" -c "./Classifier/classifier.pkl" -p "./Pipeline/pipeline.pkl" -v "./Vectorizer/vectorizer.pkl" -m "MultinomialNB_Most_Important_Features.txt"

def main(argv):

	infile = ''
	classifier_file=''
	pipeline_file=''
	vectorizer_file=''
	most_important_feature_path=''

	try:
	    opts, args = getopt.getopt(argv,"hi:c:p:v:m:",["ifile=","cfile=", "pfile=", "vfile=", "mfile="])
	except getopt.GetoptError:
	    print 'MultinomialNB_Trainer.py -i <infile> -c <classifier.pkl> -p <pipeline.pkl> -v <vectorizer.pkl> -m <features.txt>'
	    sys.exit(2)
	for opt, arg in opts:
			if opt == '-h':
				 print 'MultinomialNB_Trainer.py -i <infile> -c <classifier.pkl> -p <pipeline.pkl> -v <vectorizer.pkl> -m <features.txt>'
				 sys.exit()
			elif opt in ("-i", "--ifile"):
				 infile = arg
			elif opt in ("-c", "--cfile"):
				 classifier_file = arg
			elif opt in ("-p", "--pfile"):
				 pipeline_file = arg
			elif opt in ("-v", "--vfile"):
				 vectorizer_file = arg
			elif opt in ("-m", "--mfile"):
				 most_important_feature_path = arg
	print 'Infile is :', infile
	print 'Classifier is :', classifier_file
	print 'Pipeline is :', pipeline_file
	print 'Vectorizer is :', vectorizer_file
	print "Most_Important_Feature_path is:", most_important_feature_path

	print "Reading in the training file"
	### read in the data
	train = pd.read_csv(infile, header=0, \
                delimiter="\t", quoting=3)
	print "Train.shape is:", train.shape
	print "Train Column Values: ",train.columns.values


	#print out a review to check read in
	#print train["review"][0]
	#clean_review = review_to_words( train["review"][0] )
	#print clean_review

	#Get the number of reviews based on the dataframe column size
	num_reviews = train["review"].size

	#num_reviews = 2001 ### Debug code


	# Initialize an empty list to hold the clean reviews
	clean_train_reviews = []

	# Loop over each review; create an index i that goes from 0 to the length
	# of the review list and clean them
	for i in xrange( 0, num_reviews ):#num_reviews
		# Call our function for each one, and add the result to the list of
		# clean reviews
		if ( (i+1)% 1000 == 0 ):
			print  "Review %d of %d\n" % (i+1, num_reviews)
		clean_train_reviews.append( review_to_words( train["review"][i] ) )

	#### create the data array and the target array
	print "Creating the data and target arrays"

	target_array=[]
	data_array=[]

	#data_array = [["0" for x in range(1)] for x in range(num_reviews)]

	for i in range(0, num_reviews):
		target_array.append(train["sentiment"][i]) #set the target_array matrix which is 1 x num_reviews
		#data_array[i]= clean_train_reviews[i] # set the data array which is size num_reviews x 1
		data_array.append(clean_train_reviews[i])

	target_array2 = np.asarray(target_array)
	data_array2 = np.asarray(data_array)
	print "Target Array Shape:",target_array2.shape
	print "Data Array Shape", data_array2.shape

	#We can now quickly sample a training set while holding out 40% of the data for testing (evaluating) our classifier:
	print "Training on 60% of Data.. Results Below"


	X_train, X_test, y_train, y_test = cross_validation.train_test_split(data_array2, target_array2, test_size=0.4, random_state=0)

	print "Xtrain, ytrain shape:", X_train.shape, y_train.shape

	print "Xtest shape, y test shape:", X_test.shape, y_test.shape



	print "Creating the pipeline...\n"
	text_clf = Pipeline([('vect', CountVectorizer()),
                  ('tfidf', TfidfTransformer()),
                  ('clf', MultinomialNB()),])
	print "Fitting the target"

	text_clf = text_clf.fit(X_train, y_train)
	print "Score is:", text_clf.score(X_test, y_test)
	score_to_write=str(text_clf.score(X_test, y_test) )### variable to write this to file
	predicted = text_clf.predict(X_test)
	print "Mean is:", np.mean(predicted == y_test)
	print "Confusion Matrix:"
	print metrics.classification_report(y_test, predicted)
	confusion_matrix = str(metrics.classification_report(y_test, predicted))
	print "Writing Scores to File..."
	writeScoresToFile(most_important_feature_path, score_to_write, confusion_matrix)




#		###conduct Grid Search for parameter tuning... n_jobs allowed you to specify number of cores . -1 uses all cores
#		print "Performing Grid Search"
#
#		parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
#               'tfidf__use_idf': (True, False),
#               'clf__alpha': (1e-2, 1e-3), }

#		gs_clf = GridSearchCV(text_clf, parameters, n_jobs=-1)

#		### use a smaller subset of the data to train

#		#gs_clf = gs_clf.fit(data_array2[:400], target_array2[:400])
#		gs_clf = gs_clf.fit(X_train, y_train)

#		best_parameters, score, _ = max(gs_clf.grid_scores_, key=lambda x: x[1])
#		for param_name in sorted(parameters.keys()):
#		    print("%s: %r" % (param_name, best_parameters[param_name]))

#		print "Grid Search score", score
#### uncomment above for grid search

	### Now train on all the data
	print "Training on all the data...\n"
	final_pipeline = Pipeline([('vect', CountVectorizer()),
                  ('tfidf', TfidfTransformer()),
                  ('clf', MultinomialNB()),])
	print "Fitting the target"

	final_pipeline = final_pipeline.fit(data_array2, target_array2)
	### output results on the entire training set
	print "Outputing Results from training and predicting on the entire data... Note not a separate test set... be wary of overfitting on the results"
	print "Score is:", final_pipeline.score(data_array2, target_array2)
	score_to_write2 = str(final_pipeline.score(data_array2, target_array2) )
	predicted = final_pipeline.predict(data_array2)
	print "Mean is:", np.mean(predicted == target_array2)
	print "Confusion Matrix:"
	print metrics.classification_report(target_array2, predicted)
	confusion_matrix2 = str(metrics.classification_report(target_array2, predicted))
	print "Writing Training on all Sets to File"
	writeScoresToFile(most_important_feature_path, score_to_write2, confusion_matrix2)



	####write all the files to disk

	final_vectorizer = final_pipeline.named_steps['vect'] #for saving the vectorizer to file

	final_clf = final_pipeline.named_steps['clf'] #for saving the classifier to file
#		#### Save the classifier, pipeline, and vectorizer to file
	savePipelineToFile(final_pipeline, pipeline_file)
	saveClassifierToFile(final_clf, classifier_file)
	saveVectorizerToFile(final_vectorizer, vectorizer_file)
	writeMostImportantFeaturesToFile(final_clf, final_vectorizer, most_important_feature_path )
	print "Job Complete"

def writeScoresToFile(file_path, score, matrix):
    f=open(file_path, "a")
    f.write("Traing on the set.. be aware of overfitting on full sets \n")
    f.write("Score :" +score +"\n")
    f.write(matrix + "\n")
    f.close()

def writeMostImportantFeaturesToFile(clf, count_vect, features_path):
	### Clf most be fitted first
	### get the feature names

	feature_names = count_vect.get_feature_names()
	# Sort the coef_ as per feature weights and select largest 20 of them
	# 0 shows that we are considering the first class

	inds_pos = np.argsort(clf.coef_[0, :])[-20:]# get coefficients for the first class

	# Now, just iterate over all these indices and get the corresponding
	# feature names
	f = open(features_path, 'a')

	print "The 20 most important features for the positive class.."
	for i in inds_pos:
		print feature_names[i]
		f.write(feature_names[i]+'\n')
	f.close()
	print "Most Import Features written to file:", features_path



def saveClassifierToFile(clf, classifier_path):
	joblib.dump(clf, classifier_path) # 'filename.pkl'
	print "Classifier saved as ", classifier_path
	print "Classifier type was :", type(clf)


def saveVectorizerToFile(vectorizer, dictionary_filepath):
	joblib.dump(vectorizer.vocabulary_, dictionary_filepath)
	print "Vectorizer saved as ", dictionary_filepath
	print "Vectorizer type was :", type(vectorizer)

def savePipelineToFile(pipeline, pipeline_path):
	joblib.dump(pipeline, pipeline_path) # 'filename.pkl'
	print "Pipeline saved as ", pipeline_path
	print "Pipeline type was :", type(pipeline)

def review_to_words( raw_review ):
    # Function to convert a raw review to a string of words
    # The input is a single string (a raw movie review), and
    # the output is a single string (a preprocessed movie review)
    #
    # 1. Remove HTML
    review_text = BeautifulSoup(raw_review).get_text()
    #
    # 2. Remove non-letters
    letters_only = re.sub("[^a-zA-Z]", " ", review_text)
    #
    # 3. Convert to lower case, split into individual words
    words = letters_only.lower().split()
    #
    # 4. In Python, searching a set is much faster than searching
    #   a list, so convert the stop words to a set
    stops = set(stopwords.words("english"))
    #
    # 5. Remove stop words
    meaningful_words = [w for w in words if not w in stops]
    #
    # 6. Join the words back into one string separated by space,
    # and return the result.
    return( " ".join( meaningful_words ))

if __name__ == "__main__":
   main(sys.argv[1:])
